# eroticas

"Eróticas" es un anagrama de la "Cortesía".
Los procesos con bajo nivel de cortesía reclaman más atención del procesador.

Luego de haber probado decenas de distribuciones no he encontrado ni una sola que permita escuchar música sin entrecortes.
Por lo que inspirado en "Ananicy" decidí hacer este pequeño programa.

Este programa escrito en bash es para sistemas Linux, cambia planificadores, cortesía y niveles de prioridad según el perfil establecido por el usuario.

La idea es poder escuchar música sin que el audio amargue la vida con derrapes y entrecortes cuando un programa reclama atención del procesador.


## Modo de uso

Para probar el programa, evitar el bucle y ejcutarlo una única vez:

    ./eroticas.sh --salir

El programa requiere privilegios para funcionar:

    su -c "./eroticas.sh"

Si usas sudo:

    sudo ./eroticas.sh

Si usas root:

    ./eroticas.sh

Sin privilegios el programa podría cambiar niveles de cortesía y prioridad por valores bajos.


## Características

* Permite reutilizar perfiles.
* Permite uso de variables.
* Permite un único uso o bucle infinito.
* Usa un único archivo para todos los perfiles.
* Liviano, escrito en bash.
* Es simple, se establece la prioridad, opcionalmente un planificador y listo.
* Mínimas dependencias.

## ¿Cómo funciona?

El programa carga en memoria una lista de perfiles definidos por el usuario.
Cada minuto (o un período definido) el programa busca en la lista de procesos activos si algúno coincide con el nombre de un perfil.
El usuario debe definir bloques de perfiles. Cuando el nombre de un perfil coincide con un proceso entonces se aplica la configuración.
Por ejemplo:

    perfil=strawberry
    prioridad=-5

Cuando se encuentre a strawberry le aplicará un nivel de cortesía de -5

## Parámetros

* perfil: Es el nombre del bloque de perfil, debe ser el primero en aparecer en el bloque. Tiene que coincidir con el nombre del proceso que se quiere aplicar. También puede tener un nombre genérico para otros perfiles puedan copiar las propiedades ya definidas y así evitar repeticiones.
* planificador: Es un valor numérico del 0 al 3 que indica si aplica el planificador NORMAL, TR, RR o LOTE.
* prioridad: Es un valor numérico. Si el planificador es TR, RR o ISO el rango es de 1 a 99, si es NORMAL o LOTE el valor es de 19 a -20. El cambio a un planificador con niveles de prioridad exige este parámetro.
* planificador_io=Es un valor númericoo del 0 al 3 que indica si el planificador de entrada y salida será NORMAL, TIEMPO REAL, MEJOR ESFUERZO o INACTIVO.
* prioridad_io: Es un valor numérico del 0 al 7 que indica la prioridad de entrada y salid. Los planificadores bajo y normal no usan niveles de prioridad. Si usa un planificador bajo o normal y agrega un nivel de prioridad puede que cambie el planificador automáticamente a "mejor esfuerzo".
* limite_cpu: Es una característica que aún no está implementada.
* copia: Obtiene los parámetros de otro perfil. Si hay parámetros definidos en el bloque los omite. Solo aceptará parámetros del perfil al que está copiando.


## Ejemplos

Para que veas lo simple que es...

### Uso de variables

    # Define una variable que contiene un nivel de cortesía.
    cortesia_baja=-5

    # Perfil para el programa strawberry, aplicará el nivel de cortesía definido en la variable.
    perfil=strawberry
    prioridad=$cortesia_baja    

### Copia de perfiles

    # Perfil genérico para aplicaciones de música.    
    perfil=musica
    prioridad=-10
    planificador_io=2

    # Usa el perfil "musica".
    perfil=strawberry
    copia=musica

    # También usa el perfil "musica".
    perfil=mixxx
    copia=musica


## Requisitos

* schedtool
* renice
* ionice


## Importante

El archivo de perfiles (reglas.txt) contiene perfiles a modo de ejemplo, no implica que debas usarlo.
La responsabilidad es tuya, no soy responsable de daños causados. Hice este programa para mí y lo comparto.
Algunos parámetros como "prioridad" o "prioridad_io" son exigidos y otros deben omitirse dependiendo del planificador a usar, por ejemplo si se va a usar un planificador IO que no usa niveles de prioridad se debe omitir "prioridad_io", si se va a usar un planificador que no usa cortesía pero sí usa prioridad se debe especificar "prioridad".


## Donaciones

* Libre de Publicidad.
* Libre de Rastreadores.
* Libre de Telemetría.
* Libre de Código Propietario.
* Libre...

Al no tener publicidad estos proyectos se mantienen únicamente con donaciones de los usuarios.
Siguiendo este enlace tendrás más información y también más aplicaciones.
[Más información](https://gitlab.com/javipc/mas) 

