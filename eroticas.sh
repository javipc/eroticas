#!/bin/bash

echo "                                  "
echo "   eroticas v1.2 26/07/2022       "
echo "                                  "
echo "   por Javier Martínez            "
echo "                                  "
echo "https://gitlab.com/javipc/eroticas"
echo "                                  "








# configuración
eroticas_intervalo=120



# Lector de perfiles

lista_programas=()
while read -a linea
  do

  if [[ ${linea:0:7} == "perfil=" ]]
  then
    lista_programas+=(${linea:7})        
  fi


  linea=${linea%%#*}
  linea=${linea%% *}  
  if [[ ${linea:0:1} == "#" ]]; then  continue; fi
  if [[ -z "$linea" ]];  then continue ;fi
  
  # cuando ingresa en la sección de perfiles ....
  if [[ -n "$perfil" || ${linea:0:7} == "perfil=" ]]
  then
    # Corrige nombres de variables
    linea=${linea//"-"/"_"}
    linea=${linea//"."/"_"}
    eval $linea
    # Toma valores de perfil
    eval $(echo $perfil)$linea
    echo "Regla $perfil : $(echo $perfil)$linea"
    else
    # Toma variables
    eval $linea
  fi
  

done < reglas.txt



echo ""



# constantes de uso interno

_prioridad=prioridad
_prioridad_io=prioridad_io
_planificador=planificador
_planificador_io=planificador_io
_limite_cpu=limite_cpu
_perfil=perfil
_copia=copia    

while true
 do
      # obtiene la lista de programas 
      while read -a linea 
       do
        
        if [[ -z "$linea" ]];  then continue ;fi

        # informacion del proceso
        proc_pid=${linea[0]}
        proc_pri=${linea[1]}
        proc_cpu=${linea[2]}
        proc_mem=${linea[3]}
        proc_cmd=${linea[4]}
        proc_cls=${linea[5]}
        proc_nvl=${linea[6]}


        # informacion de perfil con respecto al proceso
        proc_cmd_original=$proc_cmd
        proc_cmd=${proc_cmd%%/*}
        proc_cmd=${proc_cmd%%(*}
        proc_cmd=${proc_cmd%%:*}
        proc_cmd=${proc_cmd//"-"/"_"}
        proc_cmd=${proc_cmd//"."/"_"}
        

        # si el proceso no existe o fue eliminado por el filro, regresa
        if [[ -z "$proc_cmd" ]]; then continue; fi

        # Comprueba si el proceso ya fue modificado
        pid_modificado=$(eval echo \$$proc_cmd$proc_pid)        
        if [[ $pid_modificado == true ]]
          then
          # echo "El proceso $proc_cmd_original ya fue modificado anteriormente"
          continue
        fi
            
        
        perfil=$(eval echo \$$proc_cmd$_perfil)
        
      

        # si el proceso no tiene perfil regresa
        if [[ $perfil$_perfil == "perfil" ]]; then continue; fi

        unset p_prioridad p_prioridad_io p_limite_cpu p_planificador p_planificador_io copia
        
        p_comando=$perfil
        
        copia=$(eval echo \$$perfil$_copia)
        
        if [[ -n "$copia" ]]; then perfil=$copia; fi

        if [[ $perfil$_prioridad != "prioridad" ]];             then p_prioridad=$(eval echo \$$perfil$_prioridad); fi
        if [[ $perfil$_prioridad_io != "prioridad_io" ]];       then p_prioridad_io=$(eval echo \$$perfil$_prioridad_io); fi
        if [[ $perfil$_limite_cpu != "limite_cpu" ]];           then p_limite_cpu=$(eval echo \$$perfil$_limite_cpu); fi
        if [[ $perfil$_planificador != "planificador" ]];       then p_planificador=$(eval echo \$$perfil$_planificador); fi
        if [[ $perfil$_planificador_io != "planificador_io" ]]; then p_planificador_io=$(eval echo \$$perfil$_planificador_io); fi
        
        echo ""
        echo "proceso: $proc_cmd_original con cortesía: $proc_pri o prioridad $proc_nvl y planificador $proc_cls"
        echo "perfil: $perfil"
        echo "planificador: $p_planificador"
        echo "prioridad: $p_prioridad"      
        echo "planificador_io: $p_planificador_io"
        echo "prioridad_io: $p_prioridad_io" 
        echo "limite_cpu: $p_limite_cpu"

        
      

        # si el perfil no coincide con el comando regresa       
        if [[ $p_comando != $proc_cmd ]]; then continue ; fi                



        
        


        
        # comprueba si debe cambiar de planificador        
        if [[ -n "$p_planificador" && -n "$p_prioridad" && $proc_cls != $p_planificador ]]
          then

          if [[ $p_planificador == 1 || $p_planificador == 2 || $p_planificador == 4 ]]
            then
            # cambia el planificador y la prioridad 
            echo "Cambiando de planificador de $proc_cmd_original de $proc_cls a $p_planificador con prioridad $p_prioridad"
            echo "schedtool -M $p_planificador -p $p_prioridad $proc_pid"
            schedtool -M $p_planificador -p $p_prioridad $proc_pid
            else
            # cambia el planificador y la cortesía
            echo "Cambiando de planificador de $proc_cmd_original de $proc_cls a $p_planificador con cortesía $p_prioridad"
            echo "schedtool -M $p_planificador -n $p_prioridad $proc_pid"
            schedtool -M $p_planificador -n $p_prioridad $proc_pid
          fi
        fi
                  

        
        # si el proceso tiene  prioridad diferente a la indicada deberá cambiarlo...
        if [[ -n "$p_prioridad" && $p_prioridad != $proc_pri && $p_prioridad != $proc_nvl ]]
          then
          
          # si es un proceso TR / RR / ISO
          if [[ $p_planificador == 1 || $p_planificador == 2 || $proc_cls == 1 || $proc_cls == 2 || $p_planificador == 4 || $proc_cls == 4 ]]
            then
            echo "Cambiando prioridad de: $proc_cmd_original . Cortesía anterior: $proc_pri . Prioridad anterior: $proc_nvl . Nueva prioridad $p_prioridad"
            echo "schedtool -p $p_prioridad $proc_pid"
            schedtool -p $p_prioridad $proc_pid
            else 
            # cambia la prioridad (no el planificador)
            echo "Cambiando cortesía de: $proc_cmd_original . Cortesía anterior: $proc_pri . Prioridad anterior: $proc_nvl . Nuevo nivel de cortesía $p_prioridad"
            echo "renice -n $p_prioridad -p $proc_pid "
            renice -n $p_prioridad -p $proc_pid 

          fi                        
          echo ""
        
        fi



        

        if [[ -n "$p_planificador_io" ]]
          then          
          echo "Cambiando planificador IO de $proc_cmd_original a $p_planificador_io"
          echo "ionice -c $p_planificador_io -p $proc_pid"
          ionice -c $p_planificador_io -p $proc_pid          
        fi


        if [[ -n "$p_prioridad_io" ]]
          then          
            echo "Cambiando prioridad IO de $proc_cmd_original a $p_prioridad_io"
            echo "ionice -n $p_prioridad_io -p $proc_pid"
            ionice -n $p_prioridad_io -p $proc_pid
        fi

        echo " "

        # agrega el proceso a la lista de modificados
        eval $(echo $proc_cmd$proc_pid=true)
                
      done < <(nice -n 19 ps -C "${lista_programas[*]}" -o pid -o nice -o %cpu -o %mem -o %c -o sched -o rtprio --no-headers --sort=-%cpu )
    
  
    if [[ $1 == '--salir' ]] 
      then 
      echo "Gracias por probar este programa (:"
      break
    fi
    
    sleep $eroticas_intervalo
           
done
